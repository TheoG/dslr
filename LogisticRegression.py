import pandas as pd
import numpy as np
from multiprocessing import Process

class LogisticRegression:
	def __init__(self):
		self.lr = 0.00001
		self.epochs = 5000
		self.m = 0
		self.thetas = []
		self.dataSet = None
		self.testDataSet = None
		self.Y = []
		self.cost = float("inf")
		self.costs = []

	def readCSV(self, fileName):
		self.dataSet = pd.read_csv(fileName)

	def sigmoid(self, z):
		return (1 / (1 + np.e**(-z)))

	def dropUselessColumns(self, columns):
		self.dataSet = self.dataSet.drop(columns, axis=1)
		self.dataSet = self.dataSet.dropna(axis=0)

	def setDependantVariable(self, columnName, name):
		target = self.dataSet[columnName]
		self.dependantVariable = name
		for val in target:
			if (val == name):
				self.Y.append(1)
			else:
				self.Y.append(0)
		self.Y = np.array(self.Y).reshape(self.dataSet.shape[0], 1)

	def normalize(self):
		n = self.dataSet.shape[1]
		self.dataSet = np.array(self.dataSet)
		for i in range(0, n):
			maxValue = float('-inf')
			dataArray = self.dataSet[:,i]
			for y in range(0, len(dataArray)):
				if dataArray[y] > maxValue:
					maxValue = dataArray[y]
			dataArray = (dataArray) / (maxValue)
			self.dataSet[:,i] = dataArray

	def prediction(self, featuresArray):
		y = self.thetas[0]

		for i in range(0, len(self.thetas) - 1):
			y = y + (self.thetas[i + 1] * featuresArray[i])

		y = self.sigmoid(y)
		return np.clip(y, 0.0001, 0.9999)

	def logisticRegression(self):
		self.m = self.dataSet.shape[0]
		self.nbFeatures = self.dataSet.shape[1]
		self.thetas = [0] * (self.nbFeatures + 1)
		
		if type(self.dataSet) is not np.ndarray: 
			self.dataSet = np.array(self.dataSet)

		featuresArray = []
		for i in range(0, self.nbFeatures):
			featuresArray.append(self.dataSet[:,i].reshape(self.m, 1))

		thetas_grad = [0] * (self.nbFeatures + 1)

		totTime1=0
		totTime2=0

		self.costs = []
		while (self.epochs > 0):
			y = self.prediction(featuresArray)

			self.cost = (- np.dot(np.transpose(self.Y), np.log(y)) - np.dot(np.transpose(1 - self.Y), np.log(1 - y))) / self.m

			thetas_grad[0] = np.dot(np.ones((1, self.m)), y - self.Y) / self.m
			for i in range(1, self.nbFeatures + 1):
				thetas_grad[i] = np.dot(np.transpose(featuresArray[i - 1]), y - self.Y) / self.m

			for i in range(0, self.nbFeatures + 1):
				self.thetas[i] = (self.thetas[i] - self.lr * thetas_grad[i])[0][0]

			self.costs.append(self.cost[0][0])
			self.epochs -= 1
		return self
