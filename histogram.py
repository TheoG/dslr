import sys
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

try:
	df = pd.read_csv(sys.argv[1])
except IndexError:
	print("usage: python histogram.py <file.csv>")
	sys.exit()
except IOError as e:
	print ("Cannot open or / and read the file '" + sys.argv[1] + "'")
	sys.exit()
except (pd.errors.EmptyDataError, pd.errors.ParserError, UnicodeDecodeError) as e:
	print ("Invalid data file '" + sys.argv[1] + "'")
	sys.exit()

columnsToDrop = [
	'Index',
	'First Name',
	'Last Name',
	'Birthday',
	'Best Hand',
	'Astronomy',
	'Herbology',
	'Defense Against the Dark Arts',
	'Divination',
	'Muggle Studies',
	'Ancient Runes',
	'History of Magic',
	'Transfiguration',
	'Potions',
	'Charms',
	'Flying']

try:
	df.drop(columnsToDrop, inplace=True, axis=1)
	df.dropna(how='any', inplace=True, axis=0)
except KeyError:
	print("Wrong csv file")
	sys.exit()

x = {}
y = {}

for index, row in df.iterrows():
	if row[df.columns.values[0]] not in x:
		x[row[df.columns.values[0]]] = []
		y[row[df.columns.values[0]]] = []

	x[row[df.columns.values[0]]].append(row[df.columns.values[1]])
	y[row[df.columns.values[0]]].append(row[df.columns.values[2]])

plt.figure(df.columns.values[1])
for key in x:
	plt.hist(x[key], alpha=0.5)
plt.title(df.columns.values[1])

plt.figure(df.columns.values[2])
plt.title(df.columns.values[2])
for key in y:
	plt.hist(y[key], alpha=0.5)

plt.show()