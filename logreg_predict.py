import pandas as pd
import numpy as np
from sklearn.metrics import accuracy_score
import csv
import sys
import json
import argparse
from LogisticRegression import LogisticRegression

if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument("dataset_test", help="CSV file containing the data for the training")
	parser.add_argument("-f", "--file", help="JSON file containing weights data")
	parser.add_argument("-c", "--accuracycheck", help="Check the accuracy of the algorithm")
	args = parser.parse_args()

	lr_Gryffindor = LogisticRegression()
	lr_Hufflepuff = LogisticRegression()
	lr_Ravenclaw = LogisticRegression()
	lr_Slytherin = LogisticRegression()


	try:
		testDataSet = pd.read_csv(args.dataset_test)
	except (pd.errors.ParserError, pd.errors.EmptyDataError, FileNotFoundError, OSError) as e:
		print(str(e))
		sys.exit()

	featuresToDrop = list(testDataSet)
	featuresToKeep = ['Hogwarts House', 'Herbology', 'Astronomy', 'Ancient Runes', 'Defense Against the Dark Arts', 'Charms']
	featuresToDrop = [x for x in featuresToDrop if x not in featuresToKeep]
	testDataSet = testDataSet.drop(featuresToDrop, axis=1)
	testDataSet = testDataSet.drop(['Hogwarts House'], axis=1)

	try:
		with open(args.file if args.file else 'weight.json') as json_file:  
			data = json.load(json_file)
			lr_Gryffindor.thetas = data['SortingHat']["Gryffindor"]
			lr_Hufflepuff.thetas = data['SortingHat']["Hufflepuff"]
			lr_Ravenclaw.thetas = data['SortingHat']["Ravenclaw"]
			lr_Slytherin.thetas = data['SortingHat']["Slytherin"]
	except FileNotFoundError:
		print("Weight file error")
		sys.exit()
	except KeyError:
		print("Train this model first")
		sys.exit()
	except json.decoder.JSONDecodeError:
		print("Corrupted json file")
		sys.exit()

	results = []
	wrongIDs = []
	correct = 0
	total = 0
	testDataSet = testDataSet.fillna(0)

	n = testDataSet.shape[1]
	testDataSet = np.array(testDataSet)
	for i in range(0, n):
		maxValue = float('-inf')
		dataArray = testDataSet[:,i]
		for y in range(0, len(dataArray)):
			if dataArray[y] > maxValue:
				maxValue = dataArray[y]
		dataArray = (dataArray) / (maxValue)
		testDataSet[:,i] = dataArray

	if args.accuracycheck:
		try:
			truthDataFrame = pd.read_csv(args.accuracycheck)
		except (pd.errors.ParserError, pd.errors.EmptyDataError, FileNotFoundError, OSError) as e:
			print(str(e))
			sys.exit()

		truth = np.array(truthDataFrame)
		truthHouses = np.array(truthDataFrame.drop(['Index'], axis=1))

	f = open('houses.csv', 'w')
	f.write("Index,Hogwarts House\n")
	for i in range(0, len(testDataSet)):

		test_x = testDataSet[i]

		y_preds = []
		try :
			y_preds.append(lr_Gryffindor.prediction(test_x))
			y_preds.append(lr_Hufflepuff.prediction(test_x))
			y_preds.append(lr_Ravenclaw.prediction(test_x))
			y_preds.append(lr_Slytherin.prediction(test_x))
		except IndexError:
			print("Wrong Test file")
			sys.exit()

		maxPred = max(y_preds)
		house = ''
		if y_preds[0] == maxPred: house = 'Gryffindor'
		if y_preds[1] == maxPred: house = 'Hufflepuff'
		if y_preds[2] == maxPred: house = 'Ravenclaw'
		if y_preds[3] == maxPred: house = 'Slytherin'

		results.append(house)
		
		if args.accuracycheck:
			if truth[i][1] == house:
				correct += 1
			else:
				# print(y_preds)
				wrongIDs.append(i)
			total += 1

		f.write(str(i)+','+house+'\n')

	if args.accuracycheck:
		print("Accuracy: {}%".format(correct / total * 100))
		# print("sklearn Accuracy: {}%".format(accuracy_score(truthHouses, results) * 100))