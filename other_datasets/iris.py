import pandas as pd
import numpy as np
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import csv
import argparse
import json

import sys
sys.path.insert(0, '../')

from LogisticRegression import LogisticRegression

parser = argparse.ArgumentParser()

parser.add_argument("-v", "--verbose", help="Output additional data", action="store_true")
parser.add_argument("-f", "--file", help="JSON file where weights will be saved")
args = parser.parse_args()


lr = LogisticRegression()
lr.lr = 0.1
lr.epochs = 1000

lr.readCSV('./Iris.csv')
testDataSet = pd.read_csv("Iris_test.csv")
lr.dropUselessColumns(['Id'])
lr.setDependantVariable('Species', 'Iris-setosa')
lr.dropUselessColumns(['Species'])

testDataSet = np.array(testDataSet.dropna(axis=0))

lr.logisticRegression()

try:
	with open(args.file if args.file else 'weight.json', 'r') as json_file:
		data = json.load(json_file)
except Exception:
	data = {}		

with open(args.file if args.file else 'weight.json', 'w') as json_file:
	data['Iris'] = {}
	data['Iris']['Iris-setosa'] = lr.thetas
	json.dump(data, json_file)


results = []
for i in range(0, len(testDataSet)):

	test_x = testDataSet[i][:4]
	y_pred = lr.prediction(test_x)
	
	if args.verbose:
		print("{:20}: {:.2f}".format(testDataSet[i][4], round(y_pred, 2)))
	
	if testDataSet[i][4] == "Iris-setosa":
		results.append(y_pred > 0.5)
	else:
		results.append(y_pred < 0.5)

print("Accuracy: {:.2f}%".format((0 if len(results) == 0 else np.sum(results) / len(results) * 100)))