import pandas as pd
import numpy as np
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import csv
import argparse
import json

import matplotlib.pyplot as plt

import sys
sys.path.insert(0, '../')

from LogisticRegression import LogisticRegression

parser = argparse.ArgumentParser()

parser.add_argument("-v", "--verbose", help="Output additional data", action="store_true")
parser.add_argument("-f", "--file", help="JSON file where weights will be saved")
args = parser.parse_args()


lr = LogisticRegression()
lr.lr = 0.001
lr.epochs = 10000

lr.readCSV('./classdata_train.csv')
testDataSet = pd.read_csv("classdata_test.csv")
lr.setDependantVariable('Class', '0')
lr.dropUselessColumns(['Class'])

testDataSet = np.array(testDataSet)

lr.logisticRegression()

try:
	with open(args.file if args.file else 'weight.json', 'r') as json_file:
		data = json.load(json_file)
except Exception:
	data = {}		

with open(args.file if args.file else 'weight.json', 'w') as json_file:
	data['Random'] = {}
	data['Random']['0'] = lr.thetas
	json.dump(data, json_file)


results = []
for i in range(0, len(testDataSet)):

	test_x = testDataSet[i][1:]

	y_pred = lr.prediction(test_x)
	y_pred = 1 if y_pred < 0.1 else 0
	
	if args.verbose:
		print("{:1}: {:.2f}".format(int(testDataSet[i][0]), round(y_pred, 2)))
	
	results.append(testDataSet[i][0] == y_pred)

if args.verbose:
	print("Accuracy: {:.2f}%".format((0 if len(results) == 0 else np.sum(results) / len(results) * 100)))