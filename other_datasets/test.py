from sklearn.datasets import make_classification
from sklearn.datasets.samples_generator import make_blobs

import numpy as np

import matplotlib.pyplot as plt



X1, Y1 = make_blobs(n_samples=100, centers=2, n_features=2)

npArrayData = np.insert(X1, 0, Y1, axis=1)

C1 = npArrayData[npArrayData[:,0] == 0]
C2 = npArrayData[npArrayData[:,0] == 1]

plt.scatter(C1[:,1], C1[:,2], marker="+", s=10, c='black')
plt.scatter(C2[:,1], C2[:,2], marker="o", s=10, c='blue')
plt.show()


for data in npArrayData:
	print("{},{},{}".format(int(data[0]), data[1], data[2]))