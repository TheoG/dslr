import matplotlib.pyplot as plt
import seaborn as s
import numpy as np
import sys
import pandas as pd

try:
	df = pd.read_csv(sys.argv[1], na_values=['', np.nan])
except IndexError:
	print("usage: python pair_plot.py <file.csv>")
	sys.exit()
except IOError as e:
	print ("Cannot open or / and read the file '" + sys.argv[1] + "'")
	sys.exit()
except (pd.errors.EmptyDataError, pd.errors.ParserError, UnicodeDecodeError) as e:
	print ("Invalid data file '" + sys.argv[1] + "'")
	sys.exit()

columnsToDrop = [
	'Index',
	'First Name',
	'Last Name',
	'Birthday',
	'Best Hand']

try:
	df.drop(columnsToDrop, inplace=True, axis=1)
except KeyError:
	print("Wrong csv file")
	sys.exit()


# Remove all rows that contain empty cell from the dataset
df.dropna(how='any', inplace=True, axis=0)

try:
	s.pairplot(df, hue="Hogwarts House", markers=[".", ".", ".", "."])
except:
	print("Error in the dataset")
	sys.exit()

plt.show()