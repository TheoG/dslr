import sys
import math
import pandas as pd
import numpy as np

if len(sys.argv) < 2:
	print ("usage: python describe.py dataset.csv")
	sys.exit()

try:
	df = pd.read_csv(sys.argv[1])
except IOError as e:
	print ("Cannot open or / and read the file '" + sys.argv[1] + "'")
	sys.exit()
except (pd.errors.EmptyDataError, pd.errors.ParserError, UnicodeDecodeError) as e:
	print ("Invalid data file '" + sys.argv[1] + "'")
	sys.exit()

df = df.select_dtypes([np.number])

# Remove all rows that contain empty cell from the dataset
df.dropna(how='any', inplace=True, axis=0)

dataHeader = ['Count', 'Mean', 'Std', 'Var', 'Min', '25%', '50%', '75%', 'Max', 'Cumu']
features = []

for col in range(0, len(df.columns)):
	data = df[df.columns.values[col]].tolist()
	data = [x for x in data if str(x) != 'nan']
	data = [x for x in data if isinstance(x, float)]

	total = 0
	dataSum = 0
	minValue = math.inf
	maxValue = -math.inf
	firstQuartile = 0
	median  = 0
	thirdQuartile = 0
	for i in range (0, len(data)):
		total = total + 1
		dataSum = dataSum + data[i]
		if data[i] > maxValue:
			maxValue = data[i]
		if data[i] < minValue:
			minValue = data[i] 

	if total > 0:
		std = 0
		var = 0
		mean = dataSum / total
		cumu = dataSum
		for i in range (0, len(data)):
			std = std + (data[i] - mean) ** 2
		var = (1.0 / total) * std
		std = math.sqrt(var)
		data.sort()
		firstQuartile = data[int(len(data) / 4)]
		median = data[int(len(data) / 2)]
		thirdQuartile = data[int(len(data) / 4) + int(len(data) / 2)]

	name = df.columns.values[col]
	if total > 0:
		features.append({
			'Name': df.columns.values[col],
			'Count': total,
			'Mean': mean,
			'Std': std,
			'Var': var,
			'Min': minValue,
			'25%': firstQuartile,
			'50%': median,
			'75%': thirdQuartile,
			'Max': maxValue,
			'Cumu': cumu
		})

print('\t', '\t'.join(['{:>15.15s}'.format(lst['Name']) for lst in features]))

for i in range(0, len(dataHeader)):
	print ('{:<8.15s}'.format(dataHeader[i]), '\t'.join(['{:>15.4f}'.format(float(round(lst[dataHeader[i]], 3))) for lst in features]))