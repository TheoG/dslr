import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import sys
import pandas as pd

if len(sys.argv) < 2:
	print ("usage: python scatter_plot.csv dataset.csv")
	sys.exit()

try:
	df = pd.read_csv(sys.argv[1])
except IOError as e:
	print ("Cannot open or / and read the file '" + sys.argv[1] + "'")
	sys.exit()
except pd.errors.EmptyDataError:
	print ("Invalid data file '" + sys.argv[1] + "'")
	sys.exit()
except pd.errors.ParserError:
	print ("Invalid data file '" + sys.argv[1] + "'")
	sys.exit()
except UnicodeDecodeError:
	print ("Invalid data file '" + sys.argv[1] + "'")
	sys.exit()

columnsToDrop = [
	'Index',
	'First Name',
	'Last Name',
	'Birthday',
	'Best Hand',
	'Arithmancy',
	'Herbology',
	'Divination',
	'Muggle Studies',
	'Ancient Runes',
	'History of Magic',
	'Transfiguration',
	'Potions',
	'Care of Magical Creatures',
	'Charms',
	'Flying']

try:
	df.drop(columnsToDrop, inplace=True, axis=1)
	df.dropna(how='any', inplace=True, axis=0)
except KeyError:
	print("Wrong csv file")
	sys.exit()

x = {}
y = {}

bg_color = '#CCCCCC'
fg_color = 'black'

fig = plt.figure(facecolor=bg_color, edgecolor=fg_color)
axes = fig.add_subplot(111)
axes.patch.set_facecolor(bg_color)
axes.xaxis.set_tick_params(color=fg_color, labelcolor=fg_color)
axes.yaxis.set_tick_params(color=fg_color, labelcolor=fg_color)
for spine in axes.spines.values():
    spine.set_color(fg_color)

colors = ["#FF0000", "#00FF00", "#0000FF", "#FF00FF"]

idx = 0
plt.xlabel(df.columns.values[1])
plt.ylabel(df.columns.values[2])

sns.scatterplot(x=df.columns.values[1], y=df.columns.values[2], data=df, hue="Hogwarts House")

plt.show()
