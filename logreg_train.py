import pandas as pd
import numpy as np
import csv
import sys
import copy
import time
import json
import matplotlib.pyplot as plt
import argparse
from multiprocessing import Process, Pool

from LogisticRegression import LogisticRegression

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument("dataset_train", help="CSV file containing the data for the training")
	parser.add_argument("--draw", help="Draw and display the cost function", action="store_true")
	parser.add_argument("-v", "--verbose", help="Output additional data", action="store_true")
	parser.add_argument("-t", "--time", help="Display LogisticRegression execution time", action="store_true")
	parser.add_argument("-f", "--file", help="JSON file where weights will be saved")
	args = parser.parse_args()

	lr_Gryffindor = LogisticRegression()

	lr_Gryffindor.lr = 0.01
	lr_Gryffindor.epochs = 20000

	try:
		lr_Gryffindor.readCSV(args.dataset_train)
	except (pd.errors.ParserError, pd.errors.EmptyDataError, FileNotFoundError, OSError) as e:
		print(str(e))
		sys.exit()

	featuresToDrop = list(lr_Gryffindor.dataSet)
	featuresToKeep = ['Hogwarts House', 'Herbology', 'Astronomy', 'Ancient Runes', 'Defense Against the Dark Arts', 'Charms']
	featuresToDrop = [x for x in featuresToDrop if x not in featuresToKeep]

	lr_Gryffindor.dropUselessColumns(featuresToDrop)

	lr_Hufflepuff = copy.deepcopy(lr_Gryffindor)
	lr_Ravenclaw = copy.deepcopy(lr_Gryffindor)
	lr_Slytherin = copy.deepcopy(lr_Gryffindor)

	lr_Gryffindor.setDependantVariable('Hogwarts House', 'Gryffindor')
	lr_Hufflepuff.setDependantVariable('Hogwarts House', 'Hufflepuff')
	lr_Ravenclaw.setDependantVariable('Hogwarts House', 'Ravenclaw')
	lr_Slytherin.setDependantVariable('Hogwarts House', 'Slytherin')
	lr_Gryffindor.dropUselessColumns(['Hogwarts House'])
	lr_Hufflepuff.dropUselessColumns(['Hogwarts House'])
	lr_Ravenclaw.dropUselessColumns(['Hogwarts House'])
	lr_Slytherin.dropUselessColumns(['Hogwarts House'])

	if args.time:
		start_time = time.time()

	lr_Gryffindor.normalize()
	lr_Hufflepuff.normalize()
	lr_Ravenclaw.normalize()
	lr_Slytherin.normalize()

	pool = Pool()
	result1 = pool.apply_async(lr_Gryffindor.logisticRegression)
	result2 = pool.apply_async(lr_Hufflepuff.logisticRegression)
	result3 = pool.apply_async(lr_Ravenclaw.logisticRegression)
	result4 = pool.apply_async(lr_Slytherin.logisticRegression)
	
	lr_Gryffindor = result1.get(timeout=200)
	lr_Hufflepuff = result2.get(timeout=200)
	lr_Ravenclaw = result3.get(timeout=200)
	lr_Slytherin = result4.get(timeout=200)

	if args.time:
		stop_time = time.time()
		print("--- {:f} seconds ---".format(stop_time - start_time))

	if args.verbose:
		print("========= COSTS =========")
		print("{:10} cost: {:f}".format('Gryffindor', lr_Gryffindor.cost[0][0]))
		print("{:10} cost: {:f}".format('Hufflepuff', lr_Hufflepuff.cost[0][0]))
		print("{:10} cost: {:f}".format('Ravenclaw', lr_Ravenclaw.cost[0][0]))
		print("{:10} cost: {:f}".format('Slytherin', lr_Slytherin.cost[0][0]))
		print("=========================\n")
		print("========= THETAS =========")
		print("{:10} thetas: {}".format('Gryffindor', np.round(lr_Gryffindor.thetas, 4)))
		print("{:10} thetas: {}".format('Hufflepuff', np.round(lr_Hufflepuff.thetas, 4)))
		print("{:10} thetas: {}".format('Ravenclaw', np.round(lr_Ravenclaw.thetas, 4)))
		print("{:10} thetas: {}".format('Slytherin', np.round(lr_Slytherin.thetas, 4)))
		print("=========================")


	try:
		with open(args.file if args.file else 'weight.json', 'r') as json_file:
			data = json.load(json_file)
	except Exception:
		data = {}		

	with open(args.file if args.file else 'weight.json', 'w') as json_file:

		data['SortingHat'] = {}
		data['SortingHat']['Gryffindor'] = lr_Gryffindor.thetas
		data['SortingHat']['Hufflepuff'] = lr_Hufflepuff.thetas
		data['SortingHat']['Ravenclaw'] = lr_Ravenclaw.thetas
		data['SortingHat']['Slytherin'] = lr_Slytherin.thetas

		json.dump(data, json_file)

	if args.draw:

		plt.figure(num="Cost functions")
		plt.ylabel('Cost')
		plt.xlabel('Epochs')
		
		plt.plot(lr_Gryffindor.costs)
		plt.plot(lr_Hufflepuff.costs)
		plt.plot(lr_Ravenclaw.costs)
		plt.plot(lr_Slytherin.costs)

		
		plt.show()
